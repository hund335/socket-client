﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Socket_client
{
    /// <summary>
    /// Interaction logic for connect.xaml
    /// </summary>
    public partial class connect : Page
    {
        public connect()
        {
            InitializeComponent();
            nextbutton.Click += next;
        }

        void next(Object sender, EventArgs e)
        {
            if (!ip.Text.ToString().Equals(""))
            {

            }
            else
            {
                MessageBox.Show("Please enter an IP address!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
