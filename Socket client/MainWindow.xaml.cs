﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Socket_client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            nextbutton.Click += next;
        }

        void next(Object sender, EventArgs e)
        {
            if (!username.Text.ToString().Equals("") && username.Text.ToString().Length <= 10)
            {
                connect c = new connect();
                this.Title = c.Title;
                this.Height = c.Height;
                this.Width = c.Width;
                this.Content = c.Content;

            }
            else if(username.Text.ToString().Length > 10)
            {
                MessageBox.Show("Sorry, but your username cant be longer than 10 characters", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                MessageBox.Show("Please pick a username if you want to continue!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }
    }
}
